package ru.tsc.kitaev.tm.command;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kitaev.tm.api.service.IServiceLocator;
import ru.tsc.kitaev.tm.enumerated.Role;

public abstract class AbstractCommand {

    @NotNull
    protected IServiceLocator serviceLocator;

    public void setServiceLocator(@NotNull final IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @Nullable
    public abstract String name();

    @Nullable
    public abstract String arg();

    @Nullable
    public abstract String description();

    public abstract void execute();

    @Nullable
    public Role[] roles() {
        return null;
    }

    @Nullable
    @Override
    public String toString() {
        String result = "";
        @Nullable String name = name();
        @Nullable String description = description();
        if (name != null) result += name + " ";
        if (description != null) result += ":" + description;
        return result;
    }

}
