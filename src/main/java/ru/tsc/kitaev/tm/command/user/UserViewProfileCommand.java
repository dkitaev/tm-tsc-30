package ru.tsc.kitaev.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kitaev.tm.command.AbstractCommand;
import ru.tsc.kitaev.tm.enumerated.Role;
import ru.tsc.kitaev.tm.exception.empty.EmptyUserIdException;
import ru.tsc.kitaev.tm.model.User;

public class UserViewProfileCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "view-user";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "view user profile...";
    }

    @Override
    public void execute() {
        @Nullable final User user = serviceLocator.getAuthService().getUser();
        if (user == null) throw new EmptyUserIdException();
        System.out.println("[VIEW PROFILE]");
        System.out.println("LOGIN: " + user.getLogin());
        System.out.println("E_MAIL: " + user.getEmail());
        System.out.println("FIRST NAME: " + user.getFirstName());
        System.out.println("LAST NAME: " + user.getLastName());
        System.out.println("MIDDLE NAME: " + user.getMiddleName());
        System.out.println("[OK]");
    }

    @Nullable
    @Override
    public Role[] roles() {
        return Role.values();
    }

}
