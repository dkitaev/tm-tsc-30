package ru.tsc.kitaev.tm.component;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.tsc.kitaev.tm.command.data.BackupLoadCommand;
import ru.tsc.kitaev.tm.command.data.BackupSaveCommand;

public class Backup extends Thread {

    @NotNull
    private final Bootstrap bootstrap;

    private static final int INTERVAL = 3000;

    public Backup(@NotNull final Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
        this.setDaemon(true);
    }

    @Override
    @SneakyThrows
    public void run() {
        while (true) {
            save();
            Thread.sleep(INTERVAL);
        }
    }

    public void init() {
        load();
        start();
    }

    public void save() {
        bootstrap.runCommand(BackupSaveCommand.BACKUP_SAVE);
    }

    public void load() {
        bootstrap.runCommand(BackupLoadCommand.BACKUP_LOAD);
    }

}
